#!/bin/bash --

# Read everything at the top of this script before proceeding

# Information:
# This script installs osu! (the circle-clicking music game) in an isolated 32-bit Wine Prefix on Linux
# The initial package install (sudo passwd) and the "Initializing the Wine Prefix for osu!" (gecko/mono) steps should be the only things requiring user-input
# https://osu.ppy.sh
# https://osu.ppy.sh/forum/t/14614

# How to execute this script:
# Choose one out of the three lines below (the lines starting with wget, bash, and curl) and run it in Terminal
# Use the first line (wget) if at all unsure
# Remove the '# ' (number sign and space) from the beginning of the line

# wget 'https://gitlab.com/apjonchhe/osu.sh/raw/master/osu.sh' -O ~/'osu.sh' && chmod +x ~/'osu.sh' && ~/'osu.sh'
# bash <(curl -s https://gitlab.com/Espionage724/Linux/raw/master/Scripts/osu.sh)
# curl -L https://gitlab.com/Espionage724/Linux/raw/master/Scripts/osu.sh | bash

# Other Notes:
# Script only works as-is with Ubuntu currently
# Main package requirements are wine (normal or staging; accessible via PATH) and aria2
# For non-Ubuntu distros, remove first 4 sections (or everything between the start/stop remove comment lines) and manually install wine and aria2

echo ""
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%"
echo "%%%%% This script will install osu! for the user '$USER'"
echo "%%%%%"
echo "%%%%% If your distro is not Ubuntu-based, please review the script"
echo "%%%%%"
echo "%%%%% Please take careful note of the console output"
echo "%%%%%"
echo "%%%%% If you experience any issues with this script, please report them"
echo "%%%%%"
echo "%%%%% Script will begin in 10 seconds"
echo "%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo ""

sleep 10

##################################################
##### For non-Ubuntu distros, start removing lines here
##################################################

echo ""
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%"
echo "%%%%% Adding the Wine repository to software sources..."
echo "%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo ""

sleep 2
#sudo add-apt-repository 'ppa:wine/wine-builds' -y
#sudo dnf config-manager --add-repo https://dl.winehq.org/wine-builds/fedora/26/winehq.repo -y
#dnf config-manager --set-enabled 
echo ""
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%"
echo "%%%%% Updating software sources..."
echo "%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo ""

sleep 2
#sudo apt-get update

echo ""
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%"
echo "%%%%% Installing Required System Packages..."
echo "%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo ""

sleep 2
#sudo dnf install winehq-stable 
#sudo apt-get install winehq-staging winbind aria2 -y

##################################################
##### For non-Ubuntu distros, stop removing lines here
##################################################

echo ""
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%"
echo "%%%%% Downloading Winetricks script..."
echo "%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo ""

sleep 2
aria2c 'https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks' -d '/home/'$USER -o 'winetricks' --allow-overwrite=true
chmod +x ~/'winetricks'

echo ""
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%"
echo "%%%%% Downloading Global Assembly Cache Tool..."
echo "%%%%% (this is for .NET Framework 4.0 later on)"
echo "%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo ""

sleep 3
mkdir -p ~/'.cache/winetricks/dotnet40'
aria2c 'https://gitlab.com/Espionage724/Linux/raw/master/Wine/Files/gacutil-net40.tar.bz2' 'https://github.com/Espionage724/Linux-Stuff/raw/master/Wine/Files/gacutil-net40.tar.bz2' -d '/home/'$USER/'.cache/winetricks/dotnet40' -o 'gacutil-net40.tar.bz2' --allow-overwrite=true

echo ""
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%"
echo "%%%%% Initializing the Wine Prefix for osu!..."
echo "%%%%% (confirm download of mono and/or gecko if prompted)"
echo "%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo ""

sleep 3
mkdir -p ~/'Wine Prefixes'
WINEPREFIX=~/'Wine Prefixes/osu!' WINEARCH=win32 wineboot
sync

echo ""
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%"
echo "%%%%% Downloading osu!'s installer..."
echo "%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo ""

sleep 2
aria2c 'https://m1.ppy.sh/r/osu!install.exe' 'https://m2.ppy.sh/r/osu!install.exe' -d '/home/'$USER/'Wine Prefixes/osu!/drive_c/users/'$USER/'Temp' -o 'osu!install.exe' --check-certificate=false --allow-overwrite=true

echo ""
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%"
echo "%%%%% Sandboxing the Wine Prefix..."
echo "%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo ""

sleep 2
WINEPREFIX=~/'Wine Prefixes/osu!' ~/'winetricks' 'sandbox'

echo ""
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%"
echo "%%%%% Improving Cursor Handling for osu!..."
echo "%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo ""

sleep 2
WINEPREFIX=~/'Wine Prefixes/osu!' ~/'winetricks' 'grabfullscreen=y'

echo ""
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%"
echo "%%%%% Downloading and Installing .NET Framework 4.0..."
echo "%%%%% (you will see a lot of harmless text output)"
echo "%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo ""

sleep 3
WINEPREFIX=~/'Wine Prefixes/osu!' ~/'winetricks' --unattended 'dotnet40'

echo ""
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%"
echo "%%%%% Downloading and Installing Asian Font Support..."
echo "%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo ""

sleep 2
WINEPREFIX=~/'Wine Prefixes/osu!' ~/'winetricks' --unattended 'cjkfonts'

echo ""
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%"
echo "%%%%% Creating a menu launcher entry for osu!..."
echo "%%%%% (you may need to reload your desktop environment)"
echo "%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo ""

sleep 3
mkdir -p ~/'.local/share/applications/wine/Programs/osu!'

cat <<EOT >> ~/'.local/share/applications/wine/Programs/osu!/osu!.desktop'
[Desktop Entry]
Name=osu!
Comment=Rhythm is just a *click* away! Actually free, online, with four gameplay modes as well as a built-in editor.
Categories=Game;
Exec=env WINEDEBUG=-all WINEPREFIX='/home/$USER/Wine Prefixes/osu!' wine '/home/$USER/Wine Prefixes/osu!/drive_c/users/$USER/Local Settings/Application Data/osu!/osu!.exe'
Type=Application
StartupNotify=true
Path=/home/$USER/Wine Prefixes/osu!/drive_c/users/$USER/Local Settings/Application Data/osu!
Icon=A7AA_osu!.0
EOT

echo ""
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%"
echo "%%%%% osu! will begin installing in 20 seconds"
echo "%%%%%"
echo "%%%%% Let osu! install without interference for best results"
echo "%%%%% (let it use the default install location)"
echo "%%%%%"
echo "%%%%% It will auto-start when installation is completed"
echo "%%%%%"
echo "%%%%% To launch osu! after installation,"
echo "%%%%% use the newly-created shortcut placed in your app launcher"
echo "%%%%% (you may have to re-log or reload the desktop launcher)"
echo "%%%%%"
echo "%%%%% The launcher icon for osu! will be created after the second start"
echo "%%%%%"
echo "%%%%% To access the osu! folder for Songs and Skins handling,"
echo "%%%%% go to '/home/$USER/Wine Prefixes/osu!/drive_c/users/$USER/Local Settings/Application Data/osu!'"
echo "%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo ""

sync
sleep 20

echo ""
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%"
echo "%%%%% Starting osu!'s Installer..."
echo "%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo ""

WINEPREFIX=~/'Wine Prefixes/osu!' wine ~/'Wine Prefixes/osu!/drive_c/users/'$USER/'Temp/osu!install.exe'

echo ""
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%"
echo "%%%%% Cleaning up files and finalizing..."
echo "%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo ""

# Don't worry if this executes while osu! is installing; Linux locks files that are in-use (osu!installer.exe)

echo "..."
rm ~/'winetricks' ~/'osu!install.exe'
rm -R ~/'.cache/winetricks'
sync
echo "All done!"

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################